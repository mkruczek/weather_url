import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by RENT on 2017-06-22.
 */
public class Main {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        String city = sc.nextLine();
        String urlString = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&APPID=d2ba89f8c8804f13b22578b589a56f26";

//        String urlString = "http://api.openweathermap.org/data/2.5/weather?q=katowice&units=metric&APPID=d2ba89f8c8804f13b22578b589a56f26";

        URL url = new URL(urlString);
        HttpURLConnection http;
        http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("GET");
        http.connect();
        int responseCode = http.getResponseCode();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(http.getInputStream()));

        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
//        System.out.println(stringBuilder.toString());

        JSONObject jsonObject = new JSONObject(stringBuilder.toString());
        JSONObject main = (JSONObject) jsonObject.get("main");

        System.out.println("Temp at " + city +": " + main.get("temp"));
    }

}
